
From Resin:

License
-------

WiFi Connect is free software, and may be redistributed under the terms specified in
the [license](https://github.com/resin-io/resin-wifi-connect/blob/master/LICENSE).


# Build on Ubuntu 

nm-wifi-connect can be built on Ubuntu 18.04 outside a container:

```
apt install cargo
cargo build --release
```

It may require additional dependencies such as libdbus-1-dev.  Note that the 
version of cargo on Ubuntu is much newer than the build version we use. 

# Run

```
sudo ./target/release/nm-wifi-service
```

